#%%
from dataclasses import dataclass
from functools import partial
from itertools import groupby
from operator import itemgetter
from typing import Iterable

import numpy as np

from helpers import chainmap
from input import Input


def from_bits(bits):
    return int("".join(map(str, bits)), 2)


def bit_criteria(input, reverse, fn, idx=0):
    idx_value = itemgetter(idx)
    x = sorted(input, key=idx_value, reverse=reverse)
    g = [(k, list(v)) for k, v in groupby(x, key=idx_value)]
    values = fn(g, key=lambda x: len(x[1]))[1]

    if len(values) > 1:
        values = bit_criteria(values, reverse=reverse, fn=fn, idx=idx + 1)

    return values[0] if idx == 0 else values


@dataclass
class Measurements:
    input: Input

    @property
    def raw(self):
        return self.input.raw

    @property
    def data(self):
        return np.array(self.input.data)

    @property
    def oxygen_rating(self):
        bits = bit_criteria(self.data, reverse=True, fn=max)
        return from_bits(bits)

    @property
    def co2_rating(self):
        bits = bit_criteria(self.data, reverse=False, fn=min)
        return from_bits(bits)

    @property
    def epsilon_rate(self):
        num_bits = len(self.raw[0])
        bitmask = "1" * num_bits
        return np.bitwise_xor(int(bitmask, 2), self.gamma_rate)

    @property
    def gamma_rate(self):
        """
        If the line it sorted, then there are more 1's than 0's
          if the number is more than half of bits

        For example,
        If the bitstring is 8 bits, an even number of 0's and 1's would be
        [ 0 0 0 0 1 1 1 1] == 16
        [ 0 0 0 0 0 1 1 1] == 8  (which is less)
        [ 0 0 0 1 1 1 1 1] == 32 (which is more)
        """
        num_bits = len(self.raw) // 2
        bitmask = "1" * num_bits
        midpoint = int(bitmask, 2)

        bits = chainmap(
            self.data.transpose(),
            sorted,
            partial(map, str),
            list,
            "".join,
            partial(int, base=2),
            lambda x: 1 if x > midpoint else 0,
            str,
        )
        bitstring = "".join(bits)
        return int(bitstring, 2)


input = Input(
    day=3,
    test_input="""
    00100
    11110
    10110
    10111
    10101
    01111
    00111
    11100
    10000
    11001
    00010
    01010
    """,
    parser=lambda lines: list(chainmap(lines, list, partial(map, int), list)),
)


def process(data: Input) -> Iterable[int]:
    m = Measurements(data)

    a = m.gamma_rate * m.epsilon_rate
    b = m.oxygen_rating * m.co2_rating

    return (a, b)


a, b = process(input)
print(f"A: {a}")
print(f"B: {b}")
