#%%
from input import Input
import numpy as np
from itertools import chain

test_data = """
16,1,2,0,4,2,7,1,2,14
"""
input = Input(
    day=7,
    test_input=test_data,
    test=False,
    parser=lambda lines: [int(x) for line in lines for x in line.split(",")],
).data


def process(input, cost):
    m = max(input)
    base = list(chain(reversed(cost), [0], cost))
    rows = np.array([np.roll(base, m + x)[: m + 1] for x in input], dtype=int)
    steps = np.sum(rows, axis=0)
    return np.amin(steps)


m = max(input)
cost = list(range(1, m + 1))

print(f"A: {process(input, cost)}")
print(f"B: {process(input, np.cumsum(cost))}")
