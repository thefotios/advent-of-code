#%%
from input import Input
from itertools import groupby
from pprint import pprint as pp

test_input = """
be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce
"""

parser = lambda lines: [
    list(map(lambda x: x.split(), line.split(" | "))) for line in lines
]


def map_values(x):
    def apply_filter(data, fn):
        match = list(filter(fn, data))
        if len(match) != 1:
            raise Exception(f"Bad match: {match}")
        return [match[0], [x for x in data if x not in match]]

    def with_mask(mask):
        return lambda x: (mask & x) == mask

    values = {k: list(v) for k, v in groupby(sorted(map(set, set(x)), key=len), len)}
    mappings = [(1, 2), (4, 4), (7, 3), (8, 7)]
    mapped = {k: values.get(v, [None])[0] for k, v in mappings}

    mapped[3], fives = apply_filter(values[5], with_mask(mapped[7]))
    bd = mapped[1] ^ mapped[4]
    d = mapped[3] & bd
    mapped[5], fives = apply_filter(fives, with_mask(bd))
    mapped[2] = fives[0]

    mapped[0], sixes = apply_filter(values[6], lambda x: (d & x) != d)
    mapped[9], sixes = apply_filter(sixes, with_mask(mapped[1]))
    mapped[6] = sixes[0]

    return {"".join(sorted(v)): k for k, v in mapped.items()}


def decode(x):
    input, output = x
    mappings = map_values(input)
    return [mappings["".join(x)] for x in map(sorted, output)]


data = Input(8, test_input=test_input, test=False, parser=parser).data

decoded = list(map(decode, data))

a = lambda output: [1 for x in output for y in x if y in [1, 4, 7, 8]]
b = lambda x: map(int, map(lambda y: "".join(map(str, y)), x))

print(f"A: {sum(a(decoded))}")
print(f"B: {sum(b(decoded))}")
