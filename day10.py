#%%
from typing import Iterable
from dataclasses import dataclass, field
from input import Input
from functools import partial
from functools import reduce
from pprint import pprint as pp

input = Input(
    10,
    test=False,
    parser=lambda x: list(map(list, x)),
    test_input="""
[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]
""",
)

pairs = {
    "}": "{",
    ")": "(",
    "]": "[",
    ">": "<",
}

mismatches = []
valid = []
for row in input.data:
    chunks = []
    while len(row):
        cur, *row = row
        opening = pairs.get(cur, None)
        if opening:
            if (prev := chunks.pop()) != opening:
                mismatches.append(cur)
                break
        else:
            chunks.append(cur)

    if len(row) == 0:
        valid.append(chunks)

points = {")": 3, "]": 57, "}": 1197, ">": 25137}
a = sum(points[x] for x in mismatches)

points = {"(": 1, "[": 2, "{": 3, "<": 4}
values = list(
    sorted(
        reduce(lambda acc, x: (acc * 5) + points[x], reversed(line), 0)
        for line in valid
    )
)
b = values[len(values) // 2]

pp({"A": a, "B": b})
