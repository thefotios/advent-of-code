#%%
from input import Input

def process(lines):
    position = 0
    depth = 0
    aim = 0
    for (direction, amount) in lines:
        match direction:
            case 'forward':
                position += amount
                depth += (aim * amount)
            case 'down':
                aim += amount
            case 'up':
                aim -= amount
    return map(lambda x: x * position, [aim, depth])


test_input = """
forward 5
down 5
forward 8
up 3
down 8
forward 2
"""

data = Input(
    day=2,
    test_input=test_input,
    test=False,
    parser = lambda lines: ((a, int(b)) for a, b in map(lambda x: x.split(), lines))
).data

a, b = process(list(data))
print(f"A: {a}")
print(f"B: {b}")

# %%
