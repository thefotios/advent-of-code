#%%
from itertools import pairwise
from more_itertools import sliding_window
from functools import partial

from input import Input


def find_increasing(measurements):
    return len(
        list(filter(lambda x: x > 0, (b - a for a, b in pairwise(measurements))))
    )


def process(lines):
    a = find_increasing(lines)
    b = find_increasing(map(sum, sliding_window(lines, 3)))
    return (a, b)


test_input = """
199
200
208
210
200
207
240
269
260
263
"""

data = Input(day=1, test_input=test_input, test=False, parser=partial(map, int)).data

a, b = process(list(data))
print(f"A: {a}")
print(f"B: {b}")
