from dataclasses import dataclass, field
from pathlib import Path

from typing import Callable, Iterable, TypeAlias, TypeVar, Union

StringList: TypeAlias = Iterable[str]
T = TypeVar("T")
ParsedLines: TypeAlias = Iterable[T]


@dataclass
class Input:
    """The Input class parses and returns input for a given puzzle.

    Example:
        test_input=\"\"\"
        1
        2
        \"\"\"

        input = Input(5, test_input=test_input,
                         test=True,
                         parser = lambda lines: map(int, lines)
                     )

        print(sum(input.data))   # returns 6
    """

    day: int
    """Which day the input is for. Used for reading the input file"""
    test_input: str = ""
    """The sample data. It can be a single string \"\"\" quoted multi-line string"""
    test: bool = False
    """Whether to parse the test_input or real input"""
    parser: Callable[[StringList], ParsedLines] = lambda x: x
    """An optional parser that will be called on the lines before returning"""
    base_path: Union[Path, str] = "inputs"
    """Where to look for the input files"""
    input_path: Path = field(init=False, default="")
    """The file to read"""

    def __post_init__(self):
        self.input_file = Path(self.base_path) / f"{self.day}.txt"

    @property
    def raw(self) -> StringList:
        """
        An iterable of each line with leading/trailing whitespace/newlines removed.
        """
        data = self.test_input if self.test else self.input_file.read_text()
        return data.strip().splitlines()

    @property
    def data(self) -> ParsedLines:
        """
        An iterable of parsed lines. By default, this is the same as raw.
        If you specify a parser, its return value will be called.

        For example:
            Given a raw value of ["1", "2", "3"]
            And a parser of `lambda lines: map(int, lines)`
            Data would be [1, 2, 3]

        Note, the parser does not necessarily have to return lines 1 for 1.
            Given the raw value of ["1", "2", "3"]
            And a parser of `lambda lines: list(map(int, chain(*[x * int(x) for x in line])))`
            Data would be `[1, 2, 2, 3, 3, 3]`
        """
        return self.parser(self.raw)
