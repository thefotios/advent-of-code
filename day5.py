# %%
import re
from dataclasses import dataclass, field
from functools import partial
from itertools import chain, product
from typing import Iterable, ParamSpec, Tuple, TypeAlias
import numpy as np

from helpers import chainmap
from input import Input


IntTuple: TypeAlias = Iterable[int]


@dataclass
class Point:
    x: int
    y: int

    def inline_with(self, other: "Point") -> bool:
        return (self.x == other.x) or (self.y == other.y)


@dataclass
class Line:
    start: Point
    stop: Point

    @property
    def straight(self):
        return self.start.inline_with(self.stop)

    def points(self):
        return self.straight_points() if self.straight else self.diagonal_points()

    def straight_points(self):
        make_range = lambda a, b: [a] if a == b else range(min(a, b), max(a, b) + 1)

        x = make_range(self.start.x, self.stop.x)
        y = make_range(self.start.y, self.stop.y)

        return list(product(x, y))

    def diagonal_points(self):
        def make_range(a, b):
            step = 1 if (a < b) else -1
            return range(a, b + step, step)

        a = make_range(self.start.x, self.stop.x)
        b = make_range(self.start.y, self.stop.y)

        return list(zip(a, b))

    @classmethod
    def build(cls, points):
        a, b, c, d = points
        return cls(start=Point(a, b), stop=Point(c, d))


def shape(lines):
    points = [p for l in lines for p in (l.start, l.stop)]
    return [max(p.x for p in points) + 1, max(p.y for p in points) + 1]


make_board = lambda lines: np.zeros(shape(lines), dtype=int)


def add_points_to_board(board, lines):
    for line in lines:
        for p in line.points():
            board[p] += 1


overlaps = lambda board: len(board[np.where(board > 1)])

#%%
test_data = """
0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
"""


parser = lambda lines: list(
    chainmap(lines, re.compile(r"[\D]+").split, partial(map, int), Line.build)
)
lines = Input(day=5, test_input=test_data, test=False, parser=parser).data
board = make_board(lines)

add_points_to_board(board, filter(lambda l: l.straight, lines))
print(f"A: {overlaps(board)}")

add_points_to_board(board, filter(lambda l: not l.straight, lines))
print(f"B: {overlaps(board)}")
