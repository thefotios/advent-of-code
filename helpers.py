from functools import reduce


def chainmap(data, *args):
    return reduce(lambda acc, fn: map(fn, acc), args, data)
