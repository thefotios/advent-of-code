import networkx as nx
import numpy as np
import numpy.ma as ma
from scipy.ndimage import generic_filter

from input import Input

test_input = """
2199943210
3987894921
9856789892
8767896789
9899965678
"""

input = Input(
    9,
    test_input=test_input,
    test=False,
    parser=lambda lines: [list(map(int, line)) for line in lines],
)
data = input.data


def a(data):
    filter_stuff = lambda items: items[2] == min(items) and items[2] not in items[0:2]
    f = (
        generic_filter(
            data,
            filter_stuff,
            footprint=[[0, 1, 0], [1, 1, 1], [0, 1, 0]],
            mode="constant",
            cval=100,
        )
        == 1
    )
    minima = ma.masked_where(
        ~f,
        data,
    )
    return sum(minima.compressed() + 1)


def b(data):
    G = nx.Graph()
    for idx, val in np.ndenumerate(data):
        if val != 9:
            G.add_node(idx, value=val)
            y, x = idx
            for node in [(y - 1, x), (y, x - 1)]:
                if G.has_node(node):
                    G.add_edge(idx, node)

    return np.product(
        list(map(len, sorted(nx.connected_components(G), key=len, reverse=True)[:3]))
    )


print(f"A: {a(data)}")
print(f"B: {b(data)}")
