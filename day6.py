#%%
from input import Input
import numpy as np

test_data = "3,4,3,1,2"
input = Input(
    day=6,
    test_input=test_data,
    test=False,
    parser=lambda lines: [int(x) for line in lines for x in line.split(",")],
).data


def process(input, days):
    current = np.zeros(9, dtype=int)

    for x in input:
        current[x] += 1

    for _ in range(days):
        current = np.roll(current, -1)
        current[6] += current[-1]

    return sum(current)


print(f"A: {process(input, 80)}")
print(f"B: {process(input, 256)}")
