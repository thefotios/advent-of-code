#%%
from dataclasses import dataclass, field
from functools import partial
from input import Input

import numpy as np
import numpy.ma as ma

from helpers import chainmap
from itertools import product

test_input = """
7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7
"""

#%%
def parse_data(lines):
    def parse_board(lines):
        _, *lines = lines
        data = chainmap(lines, lambda line: line.split(), partial(map, int), list)
        return ma.masked_array(list(data))

    called_numbers, *arr = lines
    chunked = np.array_split(arr, len(arr) // 6)
    boards = {idx: board for idx, board in enumerate(map(parse_board, chunked))}
    return (list(map(int, called_numbers.split(","))), boards)


from typing import Iterable


def mark_board(board, value):
    matches = np.where(board.data == value)
    board[matches] = ma.masked
    return board


def check_board(board):
    return any(line for n in [0, 1] for line in np.all(board.mask, axis=n))


@dataclass
class Board:
    called_numbers: Iterable[int]
    boards: Iterable[ma.masked_array]
    complete_boards: Iterable[int] = field(default_factory=list)

    def run(self):
        for num, (idx, board) in product(self.called_numbers, self.boards.items()):
            board = mark_board(board, num)
            if check_board(board):
                self.mark_complete(num, idx, board)

    def mark_complete(self, num, idx, board):
        if idx not in self.complete_boards:
            self.complete_boards.append(idx)
            value = num * sum(board.compressed())
            if len(self.complete_boards) == 1:
                print(f"A: {value}")
            elif len(self.complete_boards) == len(boards):
                print(f"B: {value}")


input = Input(4, test_input=test_input, test=False, parser=parse_data)
called_numbers, boards = input.data
b = Board(called_numbers=called_numbers, boards=boards)
b.run()

# %%
